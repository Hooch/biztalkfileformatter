# BizTalk File Formatter

Extension for Visual Studio that formats BizTalk map files (.btm) on file save for easier git merge and diff.