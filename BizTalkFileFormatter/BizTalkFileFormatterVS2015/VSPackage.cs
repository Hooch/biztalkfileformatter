﻿//------------------------------------------------------------------------------
// <copyright file="VSPackage.cs" company="IT Maciej Kuśnierz">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.OLE.Interop;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.Win32;
using EnvDTE;
using System.IO;
using Harmony;
using System.Reflection;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using System.Linq;

namespace BizTalkFileFormatterVS2015
{
    /// <summary>
    /// This is the class that implements the package exposed by this assembly.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The minimum requirement for a class to be considered a valid package for Visual Studio
    /// is to implement the IVsPackage interface and register itself with the shell.
    /// This package uses the helper classes defined inside the Managed Package Framework (MPF)
    /// to do it: it derives from the Package class that provides the implementation of the
    /// IVsPackage interface and uses the registration attributes defined in the framework to
    /// register itself and its components with the shell. These attributes tell the pkgdef creation
    /// utility what data to put into .pkgdef file.
    /// </para>
    /// <para>
    /// To get loaded into VS, the package must be referred by &lt;Asset Type="Microsoft.VisualStudio.VsPackage" ...&gt; in .vsixmanifest file.
    /// </para>
    /// </remarks>
    [PackageRegistration(UseManagedResourcesOnly = true)]
    [InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)] // Info on this package for Help/About
    [Guid(VSPackage.PackageGuidString)]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "pkgdef, VS and vsixmanifest are valid VS terms")]
    [ProvideAutoLoad(UIContextGuids80.SolutionExists)]
    public sealed class VSPackage : Package
    {
        public const string PackageGuidString = "9a46dfe3-8d19-41d9-ac97-55bf6877ac39";

        private IVsActivityLog log;

        public VSPackage()
        {
            
        }

        private void InitializeSaveHook()
        {
            string btsInstallPath = Environment.GetEnvironmentVariable("BTSINSTALLPATH");
            if (btsInstallPath == null)
            {
                log.LogEntry((uint)__ACTIVITYLOG_ENTRYTYPE.ALE_ERROR, this.ToString(), "Error resolving BTSINSTALLPATH Enviroment Variable.");
                return;
            }

            string mapDllFullName = Path.Combine(btsInstallPath, @"Developer Tools\Microsoft.BizTalk.Mapper.OM.dll");
            if (!File.Exists(mapDllFullName))
            if (btsInstallPath == null)
            {
                log.LogEntry((uint)__ACTIVITYLOG_ENTRYTYPE.ALE_ERROR, this.ToString(), "Error: Microsoft.BizTalk.Mapper.OM.dll not found in BizTalk installation directory.");
                return;
            }

            Assembly mapAssembly = Assembly.LoadFrom(mapDllFullName);
            if(mapAssembly == null)
            {
                log.LogEntry((uint)__ACTIVITYLOG_ENTRYTYPE.ALE_ERROR, this.ToString(), "Error loading assembly Microsoft.BizTalk.Mapper.OM.dll.");
                return;
            }

            Type mapType = mapAssembly.GetTypes().FirstOrDefault(t => t.FullName == "Microsoft.BizTalk.Mapper.OM.Map");
            if (mapType == null)
            {
                log.LogEntry((uint)__ACTIVITYLOG_ENTRYTYPE.ALE_ERROR, this.ToString(), "Error finding type Microsoft.BizTalk.Mapper.OM.Map in Microsoft.BizTalk.Mapper.OM assembly.");
                return;
            }

            HarmonyInstance harmony = HarmonyInstance.Create("pl.itmk.BizTalkFileFormatter.vsix");
            MethodInfo originalSave = mapType.GetMethod("Save");
            MethodInfo prefixSave = AccessTools.Method(typeof(VSPackage), "patchPrefixSave");
            MethodInfo postfixSave = AccessTools.Method(typeof(VSPackage), "patchPostfixSave");

            Patches info = harmony.GetPatchInfo(originalSave);            
            if (info == null || !info.Owners.Contains("pl.itmk.BizTalkFileFormatter.vsix"))
            {
                harmony.Patch(originalSave, new HarmonyMethod(prefixSave), new HarmonyMethod(postfixSave));
            }
        }

        protected override void Initialize()
        {
            base.Initialize();

            // Get log
            log = GetService(typeof(SVsActivityLog)) as IVsActivityLog;

            // Create Hooks
            InitializeSaveHook();
        }

        public static void patchPrefixSave(/*Microsoft.BizTalk.Mapper.OM.Map __instance,*/ ref BtmSaveFileState __state, ref string strPath)
        {
            __state = new BtmSaveFileState();
            __state.OriginalFullFileName = strPath;
            __state.TempFullFileName = Path.GetTempFileName();

            strPath = __state.TempFullFileName;
        }

        public static void patchPostfixSave(/*Microsoft.BizTalk.Mapper.OM.Map __instance,*/ ref BtmSaveFileState __state, ref string strPath)
        {
            XDocument xDocument = XDocument.Load(__state.TempFullFileName);
            xDocument.Declaration = new XDeclaration("1.0", "utf-16", null);

            XmlWriterSettings writterSettings = new XmlWriterSettings();
            writterSettings.Encoding = new UnicodeEncoding(false, true);
            writterSettings.Indent = true;
            writterSettings.IndentChars = "  ";
            writterSettings.NewLineOnAttributes = false;
            writterSettings.NewLineHandling = NewLineHandling.Replace;
            writterSettings.NewLineChars = "\r\n";
            writterSettings.OmitXmlDeclaration = false;

            using (XmlWriter xmlWritter = XmlWriter.Create(__state.OriginalFullFileName, writterSettings))
            {
                xDocument.Save(xmlWritter);
            }

            File.Delete(__state.TempFullFileName);
        }
    }
}
