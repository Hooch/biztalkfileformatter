﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizTalkFileFormatterVS2015
{
    public class BtmSaveFileState
    {
        public string OriginalFullFileName { get; set; }

        public string TempFullFileName { get; set; }
    }
}
